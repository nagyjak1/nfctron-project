# Customer Management API

This project is a case study for a job application. 

## Table of Contents
- [Project Description](#project-description)
- [Setup and Installation](#setup-and-installation)
    - [Clone the Repository](#clone-the-repository)
    - [Install Dependencies](#install-dependencies)
    - [Create a .env File](#create-a-env-file)
    - [Run the Application](#run-the-application)
- [Database Configuration](#database-configuration)
- [API Endpoints](#api-endpoints)
- [API Documentation](#api-documentation)
- [Testing](#testing)
- [Seeding the Database](#seeding-the-database)

## Project Description
The project demonstrates a simple web server built with NestJS that simulates customer management operations. 
The application provides basic CRUD functionality to manage customer data, including creating, retrieving, and updating customer records. 
It is designed with best practices in mind, including the use of TypeScript, environment configuration, database integration, and automated testing.

## Setup and Installation

1. **Clone the Repository**:
   ```bash
   git clone <repository-url>
   cd <repository-folder>
   ```
   
2. **Install Dependencies**:
    ```bash
    pnpm install
    ```

3. **Create a .env File**:
   - In the root directory of your project, create a file named `.env`.
   - Open the `.env` file and add the following configuration:
     ```
     PORT=3000 
     SEEDER_COUNT=100
     ```
     - `PORT` sets the port where the application runs.
     - `SEEDER_COUNT` specifies the number of customers added during database seeding (see [Seeding the Database](#seeding-the-database)).
   - You can also optionally set up a connection to your own database (see [Database Configuration](#database-configuration)).


4. **Run the Application**:
   ```bash
   pnpm run start
   ```

## Database Configuration

- The application defaults to SQLite if no database configuration is found in the .env file.
- To use an external **PostgreSQL** database, specify the necessary environment variables in the .env file:
   ```
   DATABASE_HOST=
   DATABASE_PORT=
   DATABASE_NAME=
   DATABASE_USER=
   DATABASE_PASSWORD=
   DATABASE_SYNCHRONIZE=
   ```
   - **DATABASE_SYNCHRONIZE**: A boolean flag (`true` or `false`) that determines whether the database schema should be automatically synchronized with the application's entities on every application startup. Set this to `true` to enable schema synchronization or `false` to disable it. **Note**: Enabling schema synchronization in production environments is generally not recommended as it might lead to unintended changes in the database schema.

- **Note**: The application currently supports only PostgreSQL databases. Ensure that your environment variables are configured correctly for PostgreSQL if using an external database.

- **Note**: Running PostgreSQL locally with Docker:
  - You can run Postgres database locally using docker with the command below:
     ```
     docker run --name my-postgres-container \
     -e POSTGRES_USER=myuser \
     -e POSTGRES_PASSWORD=mypassword \
     -e POSTGRES_DB=mydatabase \
     -p 5432:5432 \
     -d postgres:latest
     ```
  - You can configure the properties here, you need to provide these to the `.env` file then.
  - Replace `myuser`, `mypassword`, and `mydatabase` with your desired PostgreSQL username, password, and database name.
  - Once the container is running, provide these values in your .env file to connect your application to this local PostgreSQL instance.
  - The `DATABASE_HOST` will be `localhost`.
  - The `DATABASE_PORT` will be `5432`.
  
## API Endpoints

The application exposes the following endpoints:

1. **GET /customers**: Retrieves a list of all customers with basic information.
2. **GET /customers/:id**: Retrieves the details of a specific customer by ID.
3. **POST /customers**: Creates a new customer.
4. **PATCH /customers/:id**: Edits an existing customer by ID.

## API Documentation

The application provides interactive API documentation through Swagger UI. This allows you to view and test all available endpoints directly from your browser.
To access the API documentation, navigate to the /api endpoint once the application is running.
For example, if your app is running locally on port 3000, you can view the documentation by opening the following URL in your browser:
   ```
   http://localhost:3000/api
   ```


## Testing

- Unit tests for the AppController and DataService are included. 
- Unit tests can be run using:
   ```bash
   pnpm run test
   ```

## Seeding the Database
- The database is seeded with random customer data on application startup.
- **Seeding is performed only if the database is empty.**
- The number of customers added can be configured via the `SEEDER_COUNT` variable in the `.env` file.
- If `SEEDER_COUNT` is set to 0 or the variable is missing, no seeding will occur.

