import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import 'dotenv/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SeederService } from "./seeder/seeder.service";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Customer } from "./entities/customer.entity";
import { Repository } from "typeorm";

async function bootstrap(): Promise<void> {
  // create app module
  const app = await NestFactory.create(AppModule);

  // loading port from .env file and checking if present
  const configService = app.get(ConfigService);
  const port = configService.get<number>('PORT');
  if (!port) throw new Error('PORT property is not set in .env file');

  // enable global validation pipe
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true, // transforms incoming plain objects into DTOs
      whitelist: true, // remove properties that are not in the DTOs
    }),
  );

  const customerRepository: Repository<Customer> = app.get(getRepositoryToken(Customer))
  const seederService: SeederService = new SeederService(customerRepository);
  await seederService.seedDatabase(configService.get<number>('SEEDER_COUNT', 0))

  // swagger configuration
  const config = new DocumentBuilder()
    .setTitle('Customers API')
    .setDescription('The customers API description')
    .setVersion('1.0')
    .addTag('customers')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // start app
  await app.listen(port);
  console.log(`App listening on port ${port}`);
}

bootstrap();
