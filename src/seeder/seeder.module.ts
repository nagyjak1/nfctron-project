import { Module } from "@nestjs/common";
import { SeederService } from "./seeder.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Customer } from "../entities/customer.entity";

@Module({
  providers: [SeederService],
  exports: [SeederService],

  // to be able to inject CustomerRepository from TypeORM
  imports: [TypeOrmModule.forFeature([Customer])]
})
export class SeederModule {}