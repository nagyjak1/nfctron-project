import { Injectable } from '@nestjs/common';
import { faker, fakerCS_CZ } from '@faker-js/faker';
import { InjectRepository } from "@nestjs/typeorm";
import { Customer } from "../entities/customer.entity";
import { Repository } from "typeorm";

@Injectable()
export class SeederService {
  id: number = 0;

  constructor(@InjectRepository(Customer) private readonly customerRepository: Repository<Customer>) {}

  async seedDatabase(count: number): Promise<void> {
    // if DB is not empty, seeding is not performed
    const customers: Customer[] = await this.customerRepository.find();
    if (customers.length != 0) {
      return;
    }
    for (let i = 0; i < count; i++) {
      const randomCustomer = this.generateRandomCustomer();
      customers.push(this.customerRepository.create(randomCustomer));
    }
    await this.customerRepository.save(customers)
  };

  generateRandomCustomer(): Partial<Customer> {
    return {
      name: fakerCS_CZ.person.firstName(),
      email: fakerCS_CZ.internet.email(),
      phone: faker.helpers.fromRegExp('+420608[0-9]{6}'),
    };
  }

}
