import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsPhoneNumber } from 'class-validator';
import { Customer } from '../entities/customer.entity';
import { Expose } from "class-transformer";

export class CustomerDto {
  @ApiProperty({
    description: 'The unique identifier of the customer',
    example: 1,
    required: true,
  })
  @Expose()
  id: number;

  @ApiProperty({
    description: 'The name of the customer',
    example: 'Jack Logan',
    type: 'string',
    required: true,
  })
  name: string;

  @ApiProperty({
    description: 'The email of the customer',
    example: 'jack.logan@example.com',
    type: 'string',
    required: true,
  })
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty({
    description: 'The phone number of the customer',
    example: '+4201234567890',
    required: false,
  })
  @IsOptional()
  @IsPhoneNumber()
  @Expose()
  phone?: string;

  constructor(partial: Partial<Customer>) {
    return Object.assign(this, partial);
  }
}
