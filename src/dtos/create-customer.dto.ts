import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class CreateCustomerDto {
  @ApiProperty({
    description:
      'The name of the customer. This field is required and must be a valid string.',
    example: 'Jack Logan',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    description:
      'The email address of the customer. Must be a valid email format.',
    example: 'jack.logan@example.com',
    required: true,
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    description:
      'The phone number of the customer. This field is optional but should be a valid phone number if provided.',
    example: '+420123456789',
    required: false,
  })
  @IsOptional()
  @IsPhoneNumber()
  phone?: string;
}
