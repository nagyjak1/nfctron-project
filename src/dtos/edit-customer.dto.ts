import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsPhoneNumber, IsString } from 'class-validator';

export class EditCustomerDto {
  @ApiProperty({
    description: 'New name of the customer. This field is optional.',
    example: 'Jack Logan',
    required: false,
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    description: 'New email of the customer. This field is optional.',
    example: 'jack.logan@example.com',
    required: false,
  })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({
    description: 'New phone number of the customer. This field is optional.',
    example: '+420123456789',
    required: false,
  })
  @IsPhoneNumber()
  @IsOptional()
  phone?: string;
}
