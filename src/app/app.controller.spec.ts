import { AppController } from './app.controller';

import { DataService } from '../data/data.service';
import { Test, TestingModule } from '@nestjs/testing';
import { expect, jest } from '@jest/globals';
import { NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from '../dtos/create-customer.dto';
import { EditCustomerDto } from '../dtos/edit-customer.dto';
import { Customer } from '../entities/customer.entity';

jest.mock('../data/data.service');

describe('AppController', () => {
  let controller: AppController;

  // mock the repository - we will change the definition of functions in each test
  const fakeDataService = {
    findAll: jest.fn(),
    findOne: jest.fn(),
    create: jest.fn(),
    edit: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: DataService,
          useValue: fakeDataService, // use the mocked version instead of original one
        },
      ],
    }).compile();

    // get the controller from created testing module
    controller = module.get<AppController>(AppController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAll should return all customers retrieved from dataService', async () => {
    const customers: Customer[] = [
      {
        id: 2,
        name: 'adc',
        email: 'adc@adc.adc',
      },
      {
        id: 0,
        name: 'adc',
        email: 'adc@adc.adc',
        phone: '+420608440979',
      },
    ];

    // mock the 'findAll' method to return customers defined above
    jest.spyOn(fakeDataService, 'findAll').mockImplementation(() => {
      return Promise.resolve(customers);
    });
    const result = await controller.findAll();
    expect(result).toEqual(customers);
  });

  it('findOne should return the customer retrieved from dataService', async () => {
    const customer: Customer = { id: 0, name: 'adc', email: 'adc@adc.adc' };

    // mock the 'findOne' method to return the defined customer
    jest.spyOn(fakeDataService, 'findOne').mockImplementation(() => {
      return Promise.resolve(customer);
    });
    const result = await controller.findOne(0);
    expect(result).toEqual(customer);
  });

  it('findOne should throw NotFoundException if customer with given id does not exist', () => {

    // mock the 'findOne' method to throw NotFoundException
    jest.spyOn(fakeDataService, 'findOne').mockImplementation(() => {
      throw new NotFoundException();
    });
    expect(() => controller.findOne(0)).toThrow(NotFoundException);
  });

  it('create should return a created customer', async () => {
    const customer: Customer = {
      id: 0,
      name: 'adc',
      email: 'adc@adc.adc',
    };

    // mock the 'create' method to return customer
    jest.spyOn(fakeDataService, 'create').mockImplementation(() => {
      return Promise.resolve(customer);
    });

    const result = await controller.create(new CreateCustomerDto());
    expect(result).toEqual(customer);
  });

  it('edit should return a edited customer', async () => {
    const customer: Customer = { id: 0, name: 'adc', email: 'adc@adc.adc' };

    // mock the 'edit' method to edit the customer and return it
    jest.spyOn(fakeDataService, 'edit').mockImplementation(() => {
      customer.name = 'edited';
      return Promise.resolve(customer);
    });
    const result = await controller.edit(0, new EditCustomerDto());
    expect(result).toEqual(customer);
  });

  it('edit should throw NotFoundException if customer with given id does not exist', () => {

    // mock the 'edit' method to throw NotFoundException
    jest.spyOn(fakeDataService, 'edit').mockImplementation(() => {
      throw new NotFoundException();
    });
    expect(() => controller.edit(0, new EditCustomerDto())).toThrow(
      NotFoundException,
    );
  });
});
