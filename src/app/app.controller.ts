import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { CreateCustomerDto } from '../dtos/create-customer.dto';
import { EditCustomerDto } from '../dtos/edit-customer.dto';
import { CustomerDto } from '../dtos/customer.dto';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DataService } from '../data/data.service';
import { SerializeInterceptor } from '../interceptors/serialize.interceptor';
import { Customer } from '../entities/customer.entity';

@UseInterceptors(new SerializeInterceptor(CustomerDto))
@ApiTags('customers')
@Controller('customers')
export class AppController {
  constructor(private readonly appService: DataService) {}

  @Get()
  @ApiOperation({ summary: 'Retrieve all customers' })
  @ApiOkResponse({
    description: 'Successful retrieval of customer list.',
    type: [CustomerDto],
  })
  findAll(): Promise<Customer[]> {
    return this.appService.findAll();
  }

  @Post()
  @ApiOperation({ summary: 'Create a new customer' })
  @ApiBody({
    type: CreateCustomerDto,
    description: 'Customer creation data',
  })
  @ApiCreatedResponse({
    description: 'Customer successfully created.',
    type: CustomerDto,
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid input data.',
  })
  create(@Body() createCustomerDto: CreateCustomerDto): Promise<Customer> {
    return this.appService.create(createCustomerDto);
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Retrieve a customer by ID' })
  @ApiParam({
    name: 'id',
    type: Number,
    description: 'Unique customer ID',
  })
  @ApiOkResponse({
    description: 'Customer found and returned.',
    type: CustomerDto,
  })
  @ApiNotFoundResponse({
    status: 404,
    description: 'Customer with the given ID does not exist.',
  })
  findOne(@Param('id', ParseIntPipe) id: number): Promise<Customer> {
    return this.appService.findOne(id);
  }

  @Patch('/:id')
  @ApiOperation({
    summary: 'Edit a customer associated with the given ID',
    description: `Allows partial updates to a customer's details. 
                  Only the fields that need to be updated should be provided in the request body. 
                  Fields that are omitted will remain unchanged.
                  Customer's ID can not be changed.`,
  })
  @ApiParam({ name: 'id', type: Number, description: 'Unique customer ID' })
  @ApiBody({
    type: EditCustomerDto,
    description:
      'Fields to update for the customer. Omitted fields will not be updated.',
  })
  @ApiOkResponse({
    description: 'Customer successfully updated.',
    type: CustomerDto,
  })
  @ApiNotFoundResponse({
    description: 'Customer with the given ID does not exist.',
  })
  edit(
    @Param('id', ParseIntPipe) id: number,
    @Body() editCustomerDto: EditCustomerDto,
  ): Promise<Customer> {
    return this.appService.edit(id, editCustomerDto);
  }
}
