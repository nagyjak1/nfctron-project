import { Module } from '@nestjs/common';
import { DataModule } from '../data/data.module';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from '../entities/customer.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SeederModule } from '../seeder/seeder.module';

@Module({
  controllers: [AppController],
  imports: [
    DataModule,
    SeederModule,

    // to be able to inject ConfigService from config library
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),

    // TypeORM (database) configuration
    // to be able to inject all created repositories
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: configService.get<string>('DATABASE_HOST')
          ? 'postgres'
          : 'sqlite', // if DATABASE_HOST is not defined, use sqlite database
        host: configService.get<string>('DATABASE_HOST'),
        database: configService.get<string>('DATABASE_HOST')
          ? configService.get<string>('DATABASE_NAME')
          : 'sqlite', // if DATABASE_HOST is not defined, the name of DB will be 'sqlite'
        port: configService.get<number>('DATABASE_PORT'),
        username: configService.get<string>('DATABASE_USER'),
        password: configService.get<string>('DATABASE_PASSWORD'),
        entities: [Customer],
        synchronize: configService.get<string>('DATABASE_HOST')
          ? configService.get<boolean>('DATABASE_SYNCHRONIZE')
          : true,
      }),
    }),
  ],
})
export class AppModule {}
