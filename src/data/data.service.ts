import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from '../entities/customer.entity';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from '../dtos/create-customer.dto';
import { EditCustomerDto } from '../dtos/edit-customer.dto';

@Injectable()
export class DataService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
  ) {}

  async create(createCustomerDto: CreateCustomerDto): Promise<Customer> {
    const customer = this.customerRepository.create(createCustomerDto);
    try {
      return await this.customerRepository.save(customer);
    } catch (error) {
      // PostgreSQL unique constraint violation error code
      if (error.code === '23505') {
        throw new ConflictException('Email already exists');
      }
      throw error;
    }
  }

  findAll(): Promise<Customer[]> {
    return this.customerRepository.find();
  }

  async findOne(id: number): Promise<Customer> {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException('Customer with the given id does not exist');
    }
    return customer;
  }

  async edit(id: number, editCustomerDto: EditCustomerDto): Promise<Customer> {
    // try to find the customer with given id
    const customer = await this.customerRepository.findOneBy({ id: id });
    // if the user does not exist, throw exception - the nestjs exception layer will handle it
    if (!customer) {
      throw new NotFoundException('Customer with the given id does not exist');
    }
    // override found customer's properties with properties from the request's body
    Object.assign(customer, editCustomerDto);
    try {
      // save the edited customer into database and return
      return await this.customerRepository.save(customer);
    } catch (error) {
      // if the error is Postgres unique violation error, throw conflict exception
      // which will be handled by nestjs exception layer
      if (error.code === '23505') {
        throw new ConflictException('Email already exists');
      }
      // rethrow all other errors
      throw error;
    }
  }
}
