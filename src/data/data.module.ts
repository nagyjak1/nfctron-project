import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from '../entities/customer.entity';
import { DataService } from './data.service';

@Module({
  providers: [DataService],
  exports: [DataService],

  // to be able to inject CustomerRepository from TypeORM
  imports: [TypeOrmModule.forFeature([Customer])],
})
export class DataModule {}
