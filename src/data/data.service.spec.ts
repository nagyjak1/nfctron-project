import { DataService } from './data.service';
import { expect, jest } from '@jest/globals';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Customer } from '../entities/customer.entity';
import { CreateCustomerDto } from '../dtos/create-customer.dto';
import { EditCustomerDto } from '../dtos/edit-customer.dto';
import { ConflictException, NotFoundException } from '@nestjs/common';

describe('DataService', () => {
  let dataService: DataService;

  // mock the repository - we will change the definition of functions in each test
  const fakeCustomerRepository = {
    find: jest.fn(),
    findOneBy: jest.fn(),
    save: jest.fn(),
    create: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DataService,
        {
          // use the fake repository when CustomerRepository is required
          provide: getRepositoryToken(Customer),
          useValue: fakeCustomerRepository,
        },
      ],
    }).compile();

    dataService = module.get<DataService>(DataService);
  });

  it('should be defined', () => {
    expect(dataService).toBeDefined();
  });

  describe('create', () => {
    it('should successfully create and return a customer', async () => {
      const createCustomerDto: CreateCustomerDto = {
        name: 'Jakub',
        email: 'jakub@example.com',
        phone: '123456789',
      };

      const customer: Customer = { ...createCustomerDto, id: 1 };

      // mock the repository's 'create' and 'save' methods
      jest.spyOn(fakeCustomerRepository, 'create').mockImplementation(() => {
        return customer;
      });
      jest.spyOn(fakeCustomerRepository, 'save').mockImplementation(() => {
        return Promise.resolve(customer);
      });

      const result = await dataService.create(createCustomerDto);

      expect(result).toEqual(customer);
      expect(fakeCustomerRepository.create).toHaveBeenCalledWith(
        createCustomerDto,
      );
      expect(fakeCustomerRepository.save).toHaveBeenCalledWith(customer);
    });

    it('should throw ConflictException when email already exists', async () => {
      const createCustomerDto: CreateCustomerDto = {
        name: 'Jakub',
        email: 'jakub@example.com',
        phone: '123456789',
      };

      // mock the repository's 'create' method
      jest
        .spyOn(fakeCustomerRepository, 'create')
        .mockImplementation((): Customer => {
          return { ...createCustomerDto, id: 1 };
        });

      // mock the repository's 'save' method to reject the promise for unique
      // constraint violation
      jest.spyOn(fakeCustomerRepository, 'save').mockImplementation(() => {
        return Promise.reject({ code: '23505' });
      });

      await expect(dataService.create(createCustomerDto)).rejects.toThrow(
        ConflictException,
      );
    });
  });

  describe('edit', () => {
    it('should successfully edit and return the customer', async () => {
      const customer: Customer = {
        id: 1,
        name: 'Jakub',
        email: 'jakub@example.com',
        phone: '123456789',
      };
      const editCustomerDto: EditCustomerDto = {
        email: 'newemail@example.com',
      };

      // mock the 'findOneBy' method to succeed and return found customer
      jest.spyOn(fakeCustomerRepository, 'findOneBy').mockImplementation(() => {
        return Promise.resolve(customer);
      });

      // mock the 'save' method to succeed and return saved edited customer
      jest.spyOn(fakeCustomerRepository, 'save').mockImplementation(() => {
        return Promise.resolve({ ...customer, ...editCustomerDto });
      });

      const result = await dataService.edit(1, editCustomerDto);

      expect(result).toEqual({ ...customer, ...editCustomerDto });
      expect(fakeCustomerRepository.findOneBy).toHaveBeenCalledWith({ id: 1 });
      expect(fakeCustomerRepository.save).toHaveBeenCalledWith({
        ...customer,
        ...editCustomerDto,
      });
    });

    it('should throw NotFoundException if customer with given id does not exist', async () => {

      // mock 'findOneBy' to return undefined when customer with given id is not found
      jest.spyOn(fakeCustomerRepository, 'findOneBy').mockImplementation(() => {
        return Promise.resolve(undefined);
      });

      await expect(dataService.edit(1, {})).rejects.toThrow(NotFoundException);
    });

    it('should throw ConflictException when email already exists during edit', async () => {
      const customer = {
        id: 1,
        name: 'Jakub',
        email: 'jakub@example.com',
        phone: '123456789',
      };
      const editCustomerDto: EditCustomerDto = {
        email: 'newemail@example.com',
      };

      jest.spyOn(fakeCustomerRepository, 'findOneBy').mockImplementation(() => {
        return Promise.resolve(customer);
      });

      // mock the 'save' method to reject the promise for unique constraint violation
      jest
        .spyOn(fakeCustomerRepository, 'save')
        .mockImplementation(() => Promise.reject({ code: '23505' }));

      await expect(dataService.edit(1, editCustomerDto)).rejects.toThrow(
        ConflictException,
      );
    });
  });
});
